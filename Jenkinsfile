#!/usr/bin/env groovy

pipeline {
  agent any
  tools {
    nodejs 'Node'
  }
  parameters {
    booleanParam(name: "TEST_WITH_COVERAGE", defaultValue: false)
  }
  stages {
    stage('Checkout') {
      steps{
        checkout scm
      }
    }
    stage('Install') {
      steps{
        sh 'npm install'
      }
    }
    stage('Build') {
      steps{
        sh 'npm run build-ci'
      }
    }
    stage('TestsWithCoverage') {
      when {
        expression {
          return params.TEST_WITH_COVERAGE == true;
        }
      }
      steps{
        sh 'npm run test-ci'
      }
    }
    stage('TestsWithoutCoverage') {
      when {
        expression {
          return params.TEST_WITH_COVERAGE == false;
        }
      }
      steps{
        sh 'npm run test-ci-without-coverage'
      }
    }
    stage('SonarCloud') {
      steps{
        withSonarQubeEnv('SonarCloud') {
          sh "npm run sonar-scanner"
        }
      }
    }
  }
}
